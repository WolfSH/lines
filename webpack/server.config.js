const ExtractTextPlugin = require('extract-text-webpack-plugin');
const webpack = require('webpack');
const nodeExternals = require('webpack-node-externals');
const path = require('path');
const CopyWebpackPlugin = require('copy-webpack-plugin')

module.exports = {
	entry: [
		'./server/server.js'
	],
	output: {
		filename: 'js/server.js',
		path: path.resolve(__dirname, '../dist')
	},
	target: 'node',
	externals: nodeExternals(),
	module: {
		loaders: [
			{
				test: /\.js$/,
				loader: 'babel-loader'
			},
			{
				test: /\.scss$/,
				use: ['css-hot-loader'].concat(ExtractTextPlugin.extract({
					fallback: 'style-loader',
					use: 'css-loader!sass-loader'
				}))
			},
		]
	},
	plugins: [
		new ExtractTextPlugin('css/main.css'),
		new CopyWebpackPlugin([
			{
				from: 'pug/',
				to: 'pug/'
			}
		])
	],
};