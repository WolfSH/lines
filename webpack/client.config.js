const webpack = require('webpack');
const path = require('path');
const ExtractTextPlugin = require('extract-text-webpack-plugin');
const HtmlWebpackPlugin = require('html-webpack-plugin');
const { GenerateSW } = require('workbox-webpack-plugin')
const CopyWebpackPlugin = require('copy-webpack-plugin')
const UglifyJsPlugin = require('uglifyjs-webpack-plugin')

module.exports = {
	entry: [
		'webpack/hot/only-dev-server',
		'./js/main.js'
	],
	output: {
		filename: 'js/application.js',
		path: path.resolve(__dirname, '../dist')
	},
	resolve: {
		modules: [
			path.resolve('./less'),
			path.resolve('./js'),
			path.join(__dirname, '../node_modules')
		]
	},
	module: {
		loaders: [
			{
				test: /\.js$/,
				loader: 'babel-loader'
			},
			{
				test: /\.scss$/,
				use: ['css-hot-loader'].concat(ExtractTextPlugin.extract({
					fallback: 'style-loader',
					use: 'css-loader!sass-loader'
				}))
			},
			{
				test: /\.pug$/,
				loader: 'pug-loader'
			}
		]
	},
	plugins: [
		new webpack.HotModuleReplacementPlugin(),
		new ExtractTextPlugin('css/main.css'),
		new HtmlWebpackPlugin({
			template: './pug/index.pug',
			hash: true
		}),

		new CopyWebpackPlugin([{
			from: './js/manifest.json',
			to: '.'
		}, {
			from: './images/',
			to: 'images/'
		}]),
		// new GenerateSW({
		// 	globDirectory: './dist/',
		// 	globPatterns: ['**/*.{html,js,css}'],
		// 	swDest: './service-worker.js',
		// 	clientsClaim: true,
		// }),
		// new UglifyJsPlugin()
	],
	devServer: {
		hot: true
	}
};
