import cors from 'cors'
import express from 'express'
import * as React from 'react'
import { renderToString } from 'react-dom/server'
import { StaticRouter } from 'react-router-dom'

import App from '../js/react-app/App'

const app = express()

const PORT = process.env.PORT || 7000

// We're going to serve up the public
// folder since that's where our
// client bundle.js file will end up.
app
	.use(cors())
	.use('/', express.static('./dist', {maxAge: '30d'}))
	//.use('/service-worker.js', express.static('./dist/service-worker.js', {maxAge: '0'}))
	.set('view engine', 'pug')
	.set("views", './pug')
	.get('*', (req, res, next) => {
		const context = {}
		const markup = renderToString(
			<StaticRouter location={req.url} context={context}>
				<App />
			</StaticRouter>
		)

		res.render('index.pug', { markup })
	})
	.listen(PORT, () => {
		console.log(`Server is listening on port: ${PORT}`)
	})
