import React from 'react';

import { storiesOf } from '@storybook/react';
import { action } from '@storybook/addon-actions';
import Cell, { STATUS, CELL_TO_COLOR } from '../js/react-app/Pages/Game/components/Cell/Cell'
import Field from '../js/react-app/Pages/Game/components/Field/Field'
import Game from '../js/react-app/Pages/Game/Game'

import { defaultField } from './fixtures/fields'

storiesOf('App', module)
	.add('app', () => <Game />)

storiesOf('Sample', module)
	.add('app', () => 
		<div>
			<div style={{
				position: 'absolute',
				width: '50%',
				height: '200px',
				background: 'gold'
			}}>

			</div>
			<div style={{
				position: 'absolute',
				right: '0',
				width: '50%',
				height: '200px',
				background: 'crimson'
			}}>

			</div>
			<div style={{
				position: 'absolute',
				top: '40px',
				right: '25%',
				left: '25%',
				width: '50%',
				height: '100px',
				background: 'rgba(225, 132, 123, .5)'
			}}>

			</div>
		</div>
	)

storiesOf('Field', module)
	.add('default', () => 
		<div style={{
			display: 'flex',
			justifyContent: 'center',
			alignItems: 'center',
			width: '400px',
		}}>
			<Field
				activeCell={[0, 0]}
				field={defaultField}
				handleCellClick={action('cell clicked')}
			/>
		</div>)

storiesOf('Cell', module)
	.add('regular cells', () => 
		<div style={{
			display: 'flex',
		}}>
			{Array(8).fill(1).map((_, i) => <Cell key={i}
				color={CELL_TO_COLOR[i]}
				handleClick={action('clicked')}
				status={STATUS.READY} />)}
		</div>
	)
	.add('active cells', () => 
		<div style={{
			display: 'flex',
		}}>
			{Array(8).fill(1).map((_, i) => <Cell key={i}
				color={CELL_TO_COLOR[i]}
				handleClick={action('clicked')}
				status={STATUS.ACTIVE} />)}
		</div>
	)
	.add('removing cells', () => 
		<div style={{
			display: 'flex',
		}}>
			{Array(8).fill(1).map((_, i) => <Cell key={i}
				color={CELL_TO_COLOR[i]}
				handleClick={action('clicked')}
				status={STATUS.REMOVING} />)}
		</div>
	)
	.add('appearing cells', () => 
		<div style={{
			display: 'flex',
		}}>
			{Array(8).fill(1).map((_, i) => <Cell key={i}
				color={CELL_TO_COLOR[i]}
				handleClick={action('clicked')}
				status={STATUS.APPEARING} />)}
		</div>
	)
