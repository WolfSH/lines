import * as R from 'ramda'

const debug = x => {
	console.log(x)
	return x
}

const reduceIndexed = R.addIndex(R.reduce)

export const CONSTS = Object.freeze({
	NEW_BALL_QTT:         3,
	MIN_LINE_LENGTH:      5,

	DEFAULT_ROW_COUNT:    9,
	DEFAULT_COL_COUNT:    9,
	DEFAULT_COLORS_COUNT: 7,

	EMPTY:                -1
})

const ACTION = {
	MOVE:           'MOVE',
	NEW_BALLS:      'NEW_BALLS',
	DRAW_NEW_BALLS: 'DRAW_NEW_BALLS',
	REMOVE_BALLS:   'REMOVE_BALLS'
}

function longEnough(x) {
	return x.length >= CONSTS.MIN_LINE_LENGTH
}

export const createGame = (listener = () => {}) => {
	let moves = []
	let newBalls

	const currentField = []

	const game = {
		score: 0,
		// moveNumber: 0
	}

	function calculateCords(pos) {
		return [Math.floor(pos / game.config.m), pos % game.config.m]
	}

//    cells manipulation
	const getFieldCell = R.curry((field, [row, col]) => {
		return R.contains(row, R.range(0, game.config.n))
			&& R.contains(col, R.range(0, game.config.m))
			? field[row * game.config.m + col]
			: null
	})

	const getCell = getFieldCell(currentField)

	const setFieldCell = R.curry((field, color, [row, col]) => {
		field[row * game.config.m + col] = color
	})

	const setCell = setFieldCell(currentField)

	const clearCell = setCell(CONSTS.EMPTY)

	const cellsComparator = R.comparator(([aRow, aCol], [bRow, bCol]) => aRow === bRow ? aCol - bCol : aRow - bRow)

	const moveCell = (start, end) => {
		const color = getCell(start)
		clearCell(start)
		setCell(color)(end)
	}

//    moves manipulation
	const isValidMove = (start, end) => getCell(start) !== CONSTS.EMPTY && getCell(end) === CONSTS.EMPTY

	function notifyListener(...args) {
		paintField()
		listener({
			...args,
			score:      game.score,
			moves:      R.clone(moves),
			field:      R.splitEvery(game.config.m)(currentField),
			newBalls,
			isGameOver: R.none(x => x === CONSTS.EMPTY)(currentField)
		})
	}

	const checkMove = ([sRow, sCol], end) => {
		const field = R.clone(currentField)
		const stack = [[sRow, sCol]]

		let cell = null

		const DIRECTIONS = [[-1, 0], [0, 1], [1, 0], [0, -1]]

		function hasReachedTheEndPoint([row, col]) {
			return DIRECTIONS.some(([rowBias, colBias]) => {
				const temp = [row + rowBias, col + colBias]
			if (R.equals(temp, end)) {
					return true
			}

				checkCell(temp, field)
				return false

				function checkCell(cell, field) {
					if (getFieldCell(field, cell) === CONSTS.EMPTY) {
						stack.push(cell)
						setFieldCell(field, '~', cell)
			}
		}
			})
			}
		while(cell = stack.pop()) {
			if (hasReachedTheEndPoint(cell)) {
				return true
			}
		}

		return false
	}

	const drawBalls = R.forEach(({ move, color }) => {
		setCell(color)(move)
	})

	function calculateScore(clusterOfLines) {
		const scoreFromLength = x => x + (x - CONSTS.MIN_LINE_LENGTH) * (x + 1 - CONSTS.MIN_LINE_LENGTH) / 2
		const calculateScoreForLine = R.compose(scoreFromLength, R.length)
		const calculateScoreForCluster = R.compose(R.sum, R.map(calculateScoreForLine))

		return R.length(clusterOfLines) * calculateScoreForCluster(clusterOfLines)
	}

	function paintField() {
		const buildField = (field, cell, index) => {
			const newline = calculateCords(index + 1)[1] !== 0 ? '' : '\n'
			return field += `${cell || '  '}|${newline}`
		}

		const buildAsciiField = reduceIndexed(buildField, '')
		const asciiField = buildAsciiField(currentField)
		console.log(asciiField)
	}

	const getFreeCellsIndexes = reduceIndexed(
		(indexes, cell, i) => cell === CONSTS.EMPTY ? [...indexes, i] : indexes, []
	)

	function getNewBalls() {
		const freeCellsIndexes = getFreeCellsIndexes(currentField)

		const newBalls = []

		const getMove = (freeCells => {
			return () => {
				const { position, color } = calculateNextBall(freeCells.length)
				const move = calculateCords(freeCells[position])
				freeCells.splice(position, 1)

				return {
					move,
					color
				}
			}

			function calculateNextBall(range) {
				const { random, floor } = Math
				const position = floor(random() * range)
				const color = floor(random() * game.config.colorsNumber)
				return {
					position,
					color
			}
			}
		})(freeCellsIndexes)

		const addBallToNewBalls = R.compose(newBalls.push.bind(newBalls), getMove)
		const newBallsQtt = R.min(CONSTS.NEW_BALL_QTT, freeCellsIndexes.length)

		R.forEach(addBallToNewBalls)(R.range(0, newBallsQtt))
		return newBalls
	}

	function relocateBalls() {
		newBalls = newBalls.map(ball => {
			if (getCell(ball.move) !== CONSTS.EMPTY) {
				const freeCells = getFreeCellsIndexes(currentField)
				if (freeCells.length === 0) {
					return ball
				}
				const position = Math.floor(Math.random() * freeCells.length)
				return {
					...ball,
					move: calculateCords(freeCells[position])
				}
			}
			return ball
		})
	}
	game.config = {}

	game.start = ({
		n = CONSTS.DEFAULT_ROW_COUNT,
		m = CONSTS.DEFAULT_COL_COUNT,
		colorsNumber = CONSTS.DEFAULT_COLORS_COUNT
	}, pausedGame = {}) => {
		game.config.n = pausedGame.field
			? pausedGame.field.length
			: n
		game.config.m = pausedGame.field
			? pausedGame.field[0].length
			: m
		// TODO: get colors number from paused game
		game.config.colorsNumber = colorsNumber

		currentField.length = 0
		if (pausedGame.field) {
			// TODO: create transform function for field
			currentField.push(...R.flatten(pausedGame.field))
		} else {
			currentField.push(...Array(n * m).fill(CONSTS.EMPTY))
		}
		moves = pausedGame.moves || []
		game.score = pausedGame.score || 0

		if (!pausedGame.field) {
			drawBalls(getNewBalls())
		}

		newBalls = pausedGame.newBalls || getNewBalls()

		notifyListener()

		return {
			score:      game.score,
			isGameOver: pausedGame.isGameOver || false,
			field:      R.splitEvery(game.config.m)(currentField),
			moves,
			newBalls
		}
	}

	const LINES_DIRECTIONS = [[0, 1], [1, 1], [1, 0], [1, -1]]
	game.findLines = (directions => {
		return function(field, [startY, startX]) {
			const result = directions.map(([stepY, stepX]) => {
				const line = [[startY, startX]]

				let x = startX + stepX
				let y = startY + stepY
				while(getCell([y, x]) !== null && getCell([y, x]) !== CONSTS.EMPTY && getCell([y, x]) === getCell([startY, startX])) {
					line.push([y, x])
					x += stepX
					y += stepY
				}

				x = startX - stepX
				y = startY - stepY
				while(getCell([y, x]) !== null && getCell([y, x]) !== CONSTS.EMPTY && getCell([y, x]) === getCell([startY, startX])) {
					line.push([y, x])
					x -= stepX
					y -= stepY
				}

				return R.sort(cellsComparator, line)
			})
			return result
		}
	})(LINES_DIRECTIONS)

	function callAfter(f, after) {
		return (...args) => {
			f(...args)
			after()
		}
	}

	function addMove(from, to) {
		moves = [...moves, {
			type: ACTION.MOVE,
			from,
			to
		}]
	}

	function addNewBalls(balls) {
		moves = [...moves, {
			type:  ACTION.NEW_BALLS,
			balls: balls.map(({ color, move }) => ({
				color,
				location: move,
			}))
		}]
	}

	function drawNewBalls(balls) {
		drawBalls(balls)
		moves = [...moves, {
			type:  ACTION.DRAW_NEW_BALLS,
			balls: balls.map(({ color, move }) => ({
				color,
				location: move,
			}))
		}]
	}

	function removeLines(lines) {
		moves = [...moves, {
			type:  ACTION.REMOVE_BALLS,
			lines: lines.map(line => line.map(cell => ({
				location: cell,
				color: currentField[cell[0] * game.config.m + cell[1]]
			})))
		}]
		R.forEach(R.forEach(clearCell))(lines)
	}

	const addMoveAndNotify = callAfter(addMove, notifyListener)
	const addNewBallsAndNotify = callAfter(addNewBalls, notifyListener)
	const drawNewBallsAndNotify = callAfter(drawNewBalls, notifyListener)
	const removeLinesAndNotify = callAfter(removeLines, notifyListener)

	function getLines(ballsToCheck) {
		const possibleLines = []
		R.forEach(({ move, color }) => {
			possibleLines.push(...game.findLines(currentField, move))
		})(ballsToCheck)

		const getLineColor = R.compose(getCell, R.head)
		const groupByColor = R.groupBy(getLineColor)

		const groupedByColor = R.compose(groupByColor, R.filter(longEnough))(possibleLines)

		const groupedByClaster = []
		const moveToGroup = line => {
			const hasIntersection = R.compose(R.not, R.isEmpty, R.intersection(line))
			const findGroup = R.compose(R.find, R.find, hasIntersection)
			let group = findGroup(groupedByClaster)
			if (group) {
				group = R.union(group, [line])
			} else {
				groupedByClaster.push([line])
			}
		}

		const iterateOverValues = R.compose(R.forEach, R.values)
		iterateOverValues(R.forEach(moveToGroup))(groupedByColor)

		R.forEach(R.forEach(R.forEach(clearCell)))(groupedByClaster)

		const lines = R.filter(longEnough)(possibleLines)
		game.score += calculateScore(lines)

		if (lines.length) {
			removeLinesAndNotify(lines)
		}
		return lines
	}

	game.makeMove = (start, end) => {
		if (isValidMove(start, end) && checkMove(start, end)) {
			const prevScore = game.score

			moveCell(start, end)
			addMoveAndNotify(start, end)
			getLines([{
				move: end,
				color: currentField[end[0] * game.config.m + end[1]]
			}])

			if (game.score === prevScore) {
				relocateBalls()
				drawNewBallsAndNotify(newBalls)
				getLines(newBalls)
				newBalls = getNewBalls()
				addNewBallsAndNotify(newBalls)
			}
			// game.moveNumber++
		}
	}

	game.undo = () => {
		if (moves.length === 0) {
			const initialBoard = game.start(game.config)
			notifyListener(initialBoard)
		} else {
			const lastMove = R.findLastIndex(R.propEq('type', ACTION.MOVE))(moves)
			const lastActions = moves.slice(lastMove, moves.length)
			moves = moves.slice(0, lastMove)

			lastActions.reverse().forEach(action => {
				switch(action.type) {
					case ACTION.MOVE: {
						revertMove(action.from, action.to)
						break
					}
					case ACTION.REMOVE_BALLS: {
						revertBallsRemoval(action.lines)
						break
					}
					case ACTION.NEW_BALLS: {
						revertNewBalls(action.balls)
						break
					}
					case ACTION.DRAW_NEW_BALLS: {
						revertDrawNewBalls(action.balls)
						break
					}
				}
			})

			notifyListener()
		}

		function revertBallsRemoval(lines) {
			game.score -= calculateScore(lines)
			R.forEach(R.forEach(({ location, color }) => {
				currentField[location[0] * game.config.m + location[1]] = color
			}))(lines)
		}

		function revertNewBalls(balls) {
			newBalls = balls.map(({color, location}) => ({color, move: location}))
		}

		function revertDrawNewBalls(balls) {
			balls.forEach(({ location, color }) => {
				currentField[location[0] * game.config.m + location[1]] = CONSTS.EMPTY
			})
			newBalls = balls.map(({color, location}) => ({color, move: location}))
		}

		function revertMove(from, to) {
			const color = currentField[to[0] * game.config.m + to[1]]
			currentField[to[0] * game.config.m + to[1]] = CONSTS.EMPTY
			currentField[from[0] * game.config.m + from[1]] = color
		}
	}

	return game
}