import * as React from 'react'

export const SettingsContext = React.createContext(
	{
		setSetting: () => {},
		settings: {
			showNextBalls:          false,
			showNextBallsPositions: false,
		}
	}
)
