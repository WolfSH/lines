import React from 'react'

import { NEXT_BALLS, NEXT_BALLS_POSITIONS } from '../feature-toggle'
import { SettingsContext } from './SettingsContext'

export class SettingsController extends React.Component {
	state = {
		settings: {}
	}

	componentDidMount() {
		const settingsFromLocalStorage = localStorage.getItem('settings')
		this.setState({
			settings: {
				...settingsFromLocalStorage
					? JSON.parse(settingsFromLocalStorage)
					: {}
			}
		})
	}

	render () {
		return (
			<SettingsContext.Provider value={{
				settings: {
					showNextBalls:          NEXT_BALLS,
					showNextBallsPositions: NEXT_BALLS_POSITIONS,
					...this.state.settings,
				},
				setSetting: (key, value) => {
					this.setState({
						settings: {
							...this.state.settings,
							[key]: value
						}
					}, () => {
						localStorage.setItem('settings', JSON.stringify(this.state.settings));
					})

				}
			}}>
				<React.Fragment>
					{this.props.children}
				</React.Fragment>
			</SettingsContext.Provider>
		)
	}
}

