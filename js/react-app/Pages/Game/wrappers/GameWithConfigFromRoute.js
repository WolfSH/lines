
import React from 'react'

import { Game } from '../Game'
import { ROW_COUNT, COLORS_NUMBER } from '../../../config'

export function GameWithConfigFromRoute(props) {
    const size = Number(props.match.params.size || ROW_COUNT)
    const amountOfColors = Number(props.match.params.amountOfColors || COLORS_NUMBER)
    return <Game size={size} amountOfColors={amountOfColors} {...props} />
}
