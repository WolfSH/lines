import '../../../../scss/main.scss'

import * as R from 'ramda'
import React from 'react'

import { CONSTS, createGame } from '../../../app/lines'
import { Glass } from '../../Component/Glass/Glass'
import { SettingsContext } from '../../Context/SettingsContext'
import { Delayer } from '../../utils/delayer'
import { Cell, CELL_TO_COLOR, STATUS } from './components/Cell/Cell'
import { Field } from './components/Field/Field'
import { Score } from './components/Score/Score'

let lines
let delayer

export class Game extends React.Component {
    static contextType = SettingsContext

    static defaultProps = {
        onStateChange: () => {}
    }

    constructor(props) {
        super(props)
        delayer = new Delayer(gameState => {
            this.setState({
                ...gameState,
                start: null,
                end:   null
            }, () => {
                props.onStateChange(this.state)
            })
        }, 100)
        lines = createGame(gameState => delayer.onItem(gameState))

        if (typeof window !== 'undefined') {
            this.state = lines.start({
                n:            props.size,
                m:            props.size,
                colorsNumber: props.amountOfColors
            }, props.savedGame)
        }
    }

    handleCellClick = (row, col) => {
        const { field, start } = this.state
        const move = field[row][col]

        if (move !== CONSTS.EMPTY) {
            this.setState({
                start: R.equals(start, [row, col]) ? null : [row, col],
                end:   null
            })
        } else if (start) {
            lines.makeMove(start, [row, col])
        }
    }

    handleRestartClick = () => {
        lines.start({
            n:            this.props.size,
            m:            this.props.size,
            colorsNumber: this.props.amountOfColors
        })
    }

    handleUndoClick() {
        lines.undo()
    }

    render () {
        if (!this.state) {
            return null
        }

        const { field, start, end, score, isGameOver, newBalls } = this.state
        const glass = isGameOver
            ? (
                <Glass>
                    Game Over
                    <div className="btn"
                        onMouseDown={this.handleRestartClick}>
                        <i className="fa fa-refresh" aria-hidden="true"></i>
                        {/*Restart*/}
                    </div>
                </Glass>)
            : null

        return (
            <div className="app">
                {glass}
                <div className="flex flex_row">
                    <div className="btn"
                        onMouseDown={this.handleUndoClick}>
                        <i className="fa fa-reply" aria-hidden="true"></i>
                    </div>
                    <div className="btn"
                        onMouseDown={this.handleRestartClick}>
                        <i className="fa fa-refresh" aria-hidden="true"></i>
                    </div>
                    <Score score={score} />
                </div>
                {
                    this.context.settings.showNextBalls &&
                    <div className="next-balls-container flex">
                        {
                            newBalls.map(({ color }, i) =>
                                <Cell
                                    key={i}
                                    handleClick={() => {}}
                                    status={STATUS.READY}
                                    color={CELL_TO_COLOR[color]}
                                />
                            )
                        }
                    </div>
                }
                <Field
                    field={field}
                    activeCell={start}
                    endCell={end}
                    newBalls={newBalls}
                    handleCellClick={this.handleCellClick} />
            </div>
        )
    }
}
