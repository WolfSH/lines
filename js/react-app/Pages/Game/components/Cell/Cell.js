import React from 'react'
import classnames from 'classnames'

import './cell.scss'

export const CELL_TO_COLOR = [
    'red',
    'blue',
    'green',
    'brown',
    'yellow',
    'indigo',
    'orange',
    'pink',
    'purple'
]

export const INCOGNITO_CELL_COLOR = 'grey'

export const STATUS = Object.freeze({
    APPEARING: 'APPEARING',
    REMOVING:  'REMOVING',
    READY:     'READY',
    ACTIVE:    'ACTIVE'
})

export const ANIMATIONS = Object.freeze({
    APPEARING: 'zoomIn',
    REMOVING:  'bounceOut',
    READY:     '',
    ACTIVE:    'bounce'
})

export function Cell({ handleClick, status, color, className = '' }) {
    const cellClassName = classnames({
        'cell__content':            true,
        'cell__content_active':     status === STATUS.ACTIVE,
        [`cell__content_${color}`]: color,

        'animated':                 true,
        'infinite':                 status === STATUS.ACTIVE,
        [ANIMATIONS.ACTIVE]:        status === STATUS.ACTIVE,
        [ANIMATIONS.APPEARING]:     status === STATUS.APPEARING,
        [ANIMATIONS.REMOVING]:      status === STATUS.REMOVING
    })

    return (
        <div
            className={classnames('cell', className)}
            onMouseDown={handleClick}
        >
            <div className={cellClassName}>
            </div>
        </div>
    )
}
