import React from 'react'

import './score.scss'

export class Score extends React.Component {
    render() {
        const { score } = this.props
        return (
            <div className="score">
                {score}
            </div>)
    }
}
