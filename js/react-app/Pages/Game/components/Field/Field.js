import './field.scss'

import React from 'react'

import { CONSTS } from '../../../../../app/lines'
import { SettingsContext } from '../../../../Context/SettingsContext'
import { Cell, CELL_TO_COLOR, INCOGNITO_CELL_COLOR, STATUS } from '../Cell/Cell'

function calculateCellStatus(row, col, activeCell, endCell, cell, prevCell) {
    if (cell !== CONSTS.EMPTY && prevCell === CONSTS.EMPTY) {
        return STATUS.APPEARING
    }

    if (cell === CONSTS.EMPTY && prevCell !== CONSTS.EMPTY) {
        return STATUS.REMOVING
    }

    activeCell = activeCell || [NaN, NaN]
    endCell = endCell || [NaN, NaN]
    if (row === activeCell[0] && col === activeCell[1]) {
        return STATUS.ACTIVE
    }
    if (row === endCell[0] && col === endCell[1]) {
        return STATUS.APPEARING
    }
    return STATUS.READY
}

export class Field extends React.Component {
    static contextType = SettingsContext

    constructor(props) {
        super(props)

        this.state = {
            field: this.getNewFieldState(props)
        }
    }

    getNewFieldState({ field, activeCell, endCell, handleCellClick }) {
        return field.map((row, i) => {
            return row.map((cell, j) => {
                const prevCell = field[i][j]
                const status = calculateCellStatus(i, j, activeCell, endCell, cell, prevCell)
                const color = status === STATUS.REMOVING ? CELL_TO_COLOR[prevCell] : CELL_TO_COLOR[cell]
                return {
                    key: `${i}-${j}`,
                    color,
                    status,
                    handleClick: () => {handleCellClick(i, j)}
                }
            })
        })
    }

    componentWillReceiveProps(newProps) {
        this.setState({
            field: this.getNewFieldState(newProps)
        })
    }

    render() {
        const { field } = this.state
        const rows = field.map((row, i) => {
            const cells = row.map((cell, j) => {
                const newBall = this.props.newBalls.find(x => x.move[0] === i && x.move[1] === j)
                const { showNextBallsPositions, showNextBalls } = this.context.settings
                const className = showNextBallsPositions && newBall
                    ? 'cell_small'
                    : ''
                const nextBallColor = showNextBalls && newBall
                    ? CELL_TO_COLOR[newBall.color]
                    : INCOGNITO_CELL_COLOR
                const color = showNextBallsPositions && newBall
                    ? nextBallColor
                    : cell.color
                return <Cell {...cell} className={className} color={color} />
            })

            return <div className="field__row" key={i}>{ cells }</div>
        })

        return (
            <div className="field">
                <div className="field__content">
                    { rows }
                </div>
            </div>
        )
    }
}
