import React from 'react'
import { Link } from 'react-router-dom'
import * as FEATURES from '../../feature-toggle'

import './main-menu.scss'

export class MainMenu extends React.Component {
    render () {
        return (
            <main className="main-menu">
                { FEATURES.RESUME && <Link
                    to="/resume"
                    className="main-menu__item"
                >
                    Resume
                </Link> }
                <Link
                    to="/new-game"
                    className="main-menu__item"
                >
                    New game
                </Link>
                <Link
                    to={`/new-game/8`}
                    className="main-menu__item"
                >
                    New 8x8 game
                </Link>
                <Link
                    to="/new-game/10"
                    className="main-menu__item"
                >
                    New 10x10 game
                </Link>

                <Link
                    to="/settings"
                    className="main-menu__item"
                >
                    Settings
                </Link>

                { FEATURES.SCORES && <Link
                    to="/scores"
                    className="main-menu__item"
                >
                    Scores
                </Link> }
                { FEATURES.HOW_TO_PLAY && <Link
                    to="/how-to-play"
                    className="main-menu__item"
                >
                    How to play
                </Link> }
            </main>
        )
    }
}
