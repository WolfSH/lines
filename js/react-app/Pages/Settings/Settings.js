import './settings.scss'

import React from 'react'
import { Link } from 'react-router-dom'

import { SettingsContext } from '../../Context/SettingsContext'

export class Settings extends React.Component {
	static contextType = SettingsContext

	handleShowNextBallsChange = e => {
		this.context.setSetting('showNextBalls', e.target.checked)
	}

	handleShowNextBallsPositionsChange = e => {
		this.context.setSetting('showNextBallsPositions', e.target.checked)
	}

	render () {
		return (
			<main className="main-menu">
				Settings
				<label className="settings__option">
					Show next balls
					<input
						type="checkbox"
						onChange={this.handleShowNextBallsChange}
						checked={this.context.settings.showNextBalls}
					/>
				</label>
				<label className="settings__option">
					Show next balls positions
					<input
						type="checkbox"
						onChange={this.handleShowNextBallsPositionsChange}
						checked={this.context.settings.showNextBallsPositions}
					/>
				</label>
				<Link
					to="/"
					className="main-menu__item"
				>
					back
				</Link>
			</main>
		)
	}
}
