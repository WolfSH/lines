import '../../scss/main.scss'

import React from 'react'
import { Route, Switch } from 'react-router-dom'

import { SettingsController } from './Context/SettingsController'
import { GameWithConfigFromRoute } from './Pages/Game/wrappers/GameWithConfigFromRoute'
import { MainMenu } from './Pages/MainMenu/MainMenu'
import { Settings } from './Pages/Settings/Settings'

const MOVES_BLOCK_SIZE = 10

function sliceArray(arr, mod) {
    const l = arr.length
    return l%mod === 0 ? arr.slice( Math.max(l-mod,0), l ) : arr.slice( Math.max(l-l%mod,0), l )
}

function keyBase(length) {
	let key = 'moves'
	return length % MOVES_BLOCK_SIZE === 0 ? key + Math.max(length-MOVES_BLOCK_SIZE, 0) : key + (length - length%MOVES_BLOCK_SIZE)
}

function saveGame(state) {
	const oldMovesLength = parseInt(localStorage.getItem('numOfMoves'))
	const {moves, ...gameWithoutMoves} = state
	const movesLength = moves.length
	if (movesLength === 0) { 
		localStorage.removeItem('gameWithoutMoves')
		localStorage.removeItem('numOfMoves')
		let i = 0;
		while (localStorage.getItem(`moves${i}`)) { localStorage.removeItem(`moves${i}`); i += MOVES_BLOCK_SIZE;}
	} 
	else {
		localStorage.setItem('gameWithoutMoves', JSON.stringify(gameWithoutMoves))
		localStorage.setItem('numOfMoves', JSON.stringify(movesLength))
		localStorage.setItem(keyBase(movesLength), JSON.stringify(sliceArray(moves, MOVES_BLOCK_SIZE)))

		if (movesLength < oldMovesLength) {
			const nextKey = keyBase(movesLength + MOVES_BLOCK_SIZE)
			localStorage.removeItem(nextKey)
		}
		
	}
}

function getMoves(x) { return localStorage.getItem(`moves${x}`) ?  JSON.parse(localStorage.getItem(`moves${x}`)) : undefined }

function getSavedGame() {
	let state = localStorage.getItem('gameWithoutMoves') || undefined
	if (state === undefined) {
		return undefined} else {
			let moves = []
			let i = 0
			while (getMoves(i)) { moves = [...moves, ...getMoves(i)];  i += MOVES_BLOCK_SIZE }
			state= {...JSON.parse(state), moves}
			return state
	}
	
}

class App extends React.Component {
	render () {
		return (
			<SettingsController>
				<Switch>
					<Route exact path='/' component={MainMenu}/>
					<Route
						path='/resume'
						component={function ResumedGame(props) {
							return (
								<GameWithConfigFromRoute
									{...props}
									savedGame={getSavedGame() ? (getSavedGame()) : undefined}
									onStateChange={saveGame}
								/>
							)
						}}
					/>
					<Route
						path='/new-game/:size?'
						component={function Game(props) {
							return (
								<GameWithConfigFromRoute
									{...props}
									onStateChange={saveGame}
								/>
							)
						}}
					/>
					<Route path='/settings' component={Settings} />
				</Switch>
			</SettingsController>
		)
	}
}

export default App
