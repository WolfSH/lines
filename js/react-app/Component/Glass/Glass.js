import React from 'react'

import './glass.scss'

export class Glass extends React.Component {
    render() {
        return (
            <div className="glass">
                {this.props.children}
            </div>)
    }
}
