// smth that saves values and push it with at least 1sec interval
export class Delayer {
    constructor(callOnNextItem, timeout) {
        this.callOnNextItem = callOnNextItem
        this.timeout = timeout
        this.items = []
        this.isLocked = true
        this.timeoutId = this.startInterval()
    }

    startInterval() {
        return setInterval(() => {
            if (this.items.length) {
                const item = this.items.shift()
                this.callOnNextItem(item)
                this.isLocked = true
            } else {
                this.isLocked = false
            }
        }, this.timeout)
    }

    onItem(item) {
        if (!this.isLocked) {
            if (!this.items.length) {
                this.callOnNextItem(item)
            }
            clearInterval(this.timeoutId)
            this.isLocked = true
            this.timeoutId = this.startInterval()
        } else {
            this.items.push(item)
        }
    }

    // onItem(item) {
    //     console.log('bimba onItem called')
    //     if (!this.isLocked) {
    //         this.callOnNextItem(item)
    //         this.isLocked = true
    //         console.log('bimba item', item, this.items)
    //         // console.time('bimba onItem')
    //         if (this.items.length) {
    //             this.timeoutId = setTimeout(() => {
    //                 if (this.items.length) {
    //                     console.log('bimba request next item')
    //                     const item = this.items.shift()
    //                     this.isLocked = false
    //                     this.onItem(item)
    //                 } else {
    //                     console.log('bimba no items to schedule')
    //                 }
    //                 // console.timeEnd('bimba onItem')
    //             }, this.timeout)
    //         } else {
    //             this.isLocked = false
    //         }

    //     } else {
    //         if (!this.items.length) {
    //             console.log('bimba empty items but locked!!!')
    //             this.isLocked = false
    //             this.callOnNextItem(item)
    //         } else {
    //             console.log('bimba push to items')
    //             this.items.push(item)
    //         }
    //     }
    // }
}