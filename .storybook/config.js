import { configure } from '@storybook/react';
import { action } from '@storybook/addon-actions';

function loadStories() {
  require('../stories');
}

configure(loadStories, module);
