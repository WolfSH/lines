// you can use this file to add your custom webpack plugins, loaders and anything you like.
// This is just the basic way to add additional webpack configurations.
// For more information refer the docs: https://storybook.js.org/configurations/custom-webpack-config

// IMPORTANT
// When you add this file, we won't add the default configurations which is similar
// to "React Create App". This only has babel loader to load JavaScript.

const path = require('path');
const ExtractTextPlugin = require('extract-text-webpack-plugin');

module.exports = {
  plugins: [
    // your custom plugins
  ],
  resolve: {
		modules: [
			path.join(__dirname, '../less'),
			path.join(__dirname, '../js'),
			'node_modules'
		]
	},
  module: {
    rules: [
      	{
			test: /\.scss$/,
			use: ['style-loader', 'css-loader', 'scss-loader']
		},
		{
			test: /\.jsx|js?$/,
			include: [
				path.resolve(__dirname, 'js')
			],
			loaders: ['babel-loader?presets[]=es2015,presets[]=stage-0,presets[]=react,plugins[]=transform-runtime'],
		}, {
			test: /\.pug$/,
			use: ['html-loader', 'pug-html-loader']
		}
    ],
  },
};
